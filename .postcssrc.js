// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": [
    require("postcss-import")(),
    require('tailwindcss')('./main-css.js'),
    require("postcss-url")(),
    require("autoprefixer")()
  ]
}