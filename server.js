const express = require('express')
const nodemailer = require('nodemailer')
const settings = require('./server/settings.json')
const bodyParser = require('body-parser')

let transporter = nodemailer.createTransport({
  host: 'localhost',
  port: 2500,
  secure: false,
})

const app = express()

app.use(express.static(__dirname + '/dist/'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/dist/index.html')
})

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/submitform', (req, res) => {
  let fullname = req.body.fullname
  let email = req.body.email
  let service = req.body.service
  let message = req.body.message

  if (fullname && emailRegex.test(email) && service && message) {
    let mailOptions = {
      from: '"BHWD LLC" <no-reply@ben-harrington.com>', // sender address
      to: 'bharrington11@gmail.com', // list of receivers
      subject: `New Inquiry from ${fullname}`, // Subject line
      text: ` Name: ${fullname} \n Email: ${email} \n Service: ${service} \n Message: ${message}`, // plain text body
      html: ` Name: ${fullname} <br/> Email: ${email} <br/> Service: ${service} <br/> Message: ${message}` // html body
    }
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        res.sendStatus(500).end()
      }
      console.log('Message sent: %s', info.messageId);
      res.sendStatus(200).end()
    })
  } else {
    console.log('Invalid request!')
    res.sendStatus(400).end()
  }
})

app.listen(3000, () => {
  console.log('Listening on 3000')
})

emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)