import Vue from 'vue'
import Router from 'vue-router'
import vueScrollwatch from 'vue-scrollwatch'
import VueLodash from 'vue-lodash'
import Home from '@/pages/Home'

Vue.use(Router)
Vue.use(vueScrollwatch)
Vue.use(VueLodash)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Home
    }
  ]
})
